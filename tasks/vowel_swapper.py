def vowel_swapper(string):
    # ==============
    # Your code here
    chr_list = list(string)
    for i, val in enumerate(chr_list):
        if val == "a" or val == "A":
            chr_list[i]= "4"
        elif val == "e" or val == "E":
            chr_list[i]= "3" 
        elif val == "i" or val == "I":
            chr_list[i]= "!"
        elif val == "o":
            chr_list[i]= "ooo"
        elif val == "O":
            chr_list[i]= "000"
        elif val == "u" or val == "U":
            chr_list[i]= "|_|"
    result = "".join(chr_list)
    return result
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console